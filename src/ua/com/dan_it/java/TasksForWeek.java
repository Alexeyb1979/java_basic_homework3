package ua.com.dan_it.java;

import java.util.Scanner;

public class TasksForWeek {
    public static void main(String[] args) {

        String[][] scedule = new String[7][2];
        Day[] day = Day.values();
        int dayIndex = 0;

        for (int i = 0; i < scedule.length; i++) {
            for (int j = 0; j < 1; j++) {
                scedule[i][j] = String.valueOf(day[dayIndex]);
//                System.out.print(scedule[i][j] = String.valueOf(day[dayIndex]));
                dayIndex++;
            }
//            System.out.println();
        }

        scedule[0][1] = "do home work";
        scedule[1][1] = "go to the courses";
        scedule[2][1] = "go to the sinema";
        scedule[3][1] = "prepearing to next lesson";
        scedule[4][1] = "go to dantist";
        scedule[5][1] = "go to swim";
        scedule[6][1] = "meeting with friends";


        boolean usersCorrectAnswer = false;
        Scanner scanner = new Scanner(System.in);


         while (usersCorrectAnswer == false) {
             System.out.println("Please, input the day of the week: ");
            String usersAnswer = scanner.next().trim().toUpperCase();
            int switchIndex = switchIndex(scedule, usersAnswer);

            switch (switchIndex) {
                case 1:
                    for (int i = 0; i < scedule.length; i++) {
                        if (scedule[i][0].equalsIgnoreCase(usersAnswer)) {
                            System.out.println("Your tasks for " + scedule[i][0] + ": " + scedule[i][1]);
                            break;
                        }
                    }
                    usersCorrectAnswer = false;
                    break;
                case 2:
                    System.out.println("Sorry, I don't understand you, please try again.");
                    usersCorrectAnswer = false;
                    break;
                case 3:
                    usersCorrectAnswer = true;
                    break;
            }
       }
    }

    enum Day {

        MONDAY,
        TUESDAY,
        WEDNESDAY,
        THURSDAY,
        FRIDAY,
        SATURDAY,
        SUNDAY
    }

    public static int switchIndex(String[][] arr, String str) {
        int ind = 0;

        boolean compare = false;
        for (int i = 0; i < arr.length; i++) {
            if (arr[i][0].equals(str) == true) {
                    compare = true;
                    break;
                }
            }

        if (str.equalsIgnoreCase("exit") == true) {
            ind = 3;
        } else if (compare == true) {
            ind = 1;
        } else {
            ind = 2;
        }
        return ind;
    }
}
